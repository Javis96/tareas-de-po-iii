#include <iostream>
#include <string.h>

using namespace std;

struct  nodo
{
	int datopArma;
	int item;
	nodo *sig = nullptr;
	nodo * atras = nullptr;
};

nodo *primero = nullptr;
nodo *ultimo = nullptr;

bool isEncontrado = false;


void insertar(int  a, int b)
{
	nodo *nuevo = new nodo();
	nuevo->datopArma = a;
	nuevo->item = b;

	if (primero == nullptr)
	{
		primero = nuevo;
		ultimo = nuevo;
		primero->sig = primero;
		primero->atras = ultimo;
	}
	else
	{
		ultimo->sig = nuevo;
		nuevo->atras = ultimo;
		nuevo->sig = primero;
		ultimo = nuevo;
		primero->atras = ultimo;

	}
}

void imprimir()
{
	nodo *aux = new nodo();
	aux = ultimo;
	if (primero != nullptr)
	{
		do
		{
			
			cout << " " << aux->datopArma << " " << aux->item << endl;
			aux = aux->atras;
		} while (aux != ultimo);

	}
	else
		cout << "no se pudo imprimir la lista";

}

void buscar(int a)
{
	nodo *aux = new nodo();
	aux = ultimo;
	if (primero != nullptr)
	{
		do
		{
			if (aux->datopArma == a)
			{
				cout << " se encontro " << aux->datopArma << endl;
				isEncontrado = true;
			}
			aux = aux->atras;
		} while (aux != ultimo && isEncontrado != true);

		if (isEncontrado == false)
			cout << "no se encontro";

	}
	else
		cout << "no se pudo imprimir la lista";

}

void modificar(int a)
{
	nodo *aux = new nodo();
	aux = ultimo;
	if (primero != nullptr)
	{
		do
		{
			if (aux->datopArma == a)
			{
				cout << " se encontro " << aux->datopArma << endl;
				cout << "Ingrese  el nuevo dato para este dato: ";
				cin >> aux->datopArma;
				isEncontrado = true;
			}
			aux = aux->atras;
		} while (aux != ultimo && isEncontrado != true);

		if (isEncontrado == false)
			cout << "no se encontro";

	}
	else
		cout << "no se pudo imprimir la lista";
}
int main()
{
	bool salir = true;
	while (salir)
	{
	int o ;

	cout << endl;
	cout << "Menu" << endl;
	cout << "1.- insertar items" << endl;
	cout << "2.- buscar el item" << endl;
	cout << "3.- imprimir items" << endl;
	cout << "4.- modificar inventario" << endl;
	cout << "5.- salir" << endl;
	cout << endl;

	cout << "que desea hacer" << endl;
	cin >> o;

	
		switch (o)
		{
		case 1:
			int n;
			cout << "cuantos Items quieres insertar: ";
			cin >> n;
			for (int  i = 0; i< n; ++i)
			{
				int a, b;
				cout << "Insertar arma: ";
				cin >> a;
				cout << "Insertar item: ";
				cin >> b;
				insertar(a, b);
			}
			break;
		case 2: 
			int b;
			cout << "que item desea buscar: ";
			cin >> b;
			buscar(b);
			break;
		case 3:
			imprimir();
			break;
		case 4:
			int a;
			cout << "Que arma desaea cambiar: ";
			cin >> a;
			modificar(a);
			break;
		case 5:
			salir = false;
			break;
		default:
			cout << "elige una opcion correcta";
		}

	}
	
	system("Pause");
	return 0;
}

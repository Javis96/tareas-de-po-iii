#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string.h>

using namespace std;


int **array_p,nRows,nCol;

void insertar()
{
	cout << "Digite el numero de filas: ";
	cin >> nRows;
	cout << "Digite el numero e columnas: ";
	cin >> nCol;

	array_p = (int**)malloc(nRows * sizeof(int));

	if (array_p == nullptr)
	{
		cout << "error en asignar memoria" << endl;
		exit(1);
	}

	for (int i = 0; i < nRows; ++i)
		array_p[i] = (int*)malloc(nCol * sizeof(int));

	cout << endl;

	cout << "Introduce los datos e arreglo bidimensional" << endl;
	cout << endl;

	for (int  i = 0; i < nRows; ++i)
	{
		for (int  j = 0; j < nCol;++j)
		{
			cout << "Digewte el numero en la posicion [" << i << "] [" << j << "]: ";
			cin >> array_p[i][j];
		}
	}
	cout << endl;
}

void imprimir(int **puntero, int i,int j)
{
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCol; ++j)
		{
			
			cout << puntero[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void guardarArreglo(int **puntero,int i,int j, std::string b)
{
	ofstream archivo;

	archivo.open(b, ios::out);
	if (archivo.fail())
	{
		cout << "no se pudo abrir el archivo";
		exit(1);
	}

	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCol; ++j)
		{

			archivo << puntero[i][j] << " ";
		}
		archivo << endl;
	}
	archivo.close();

}




int main()
{
	insertar();
	imprimir(array_p,nRows,nCol);
	guardarArreglo(array_p,nRows,nCol,"arreglo.txt");

	system("Pause");
	return 0;
}